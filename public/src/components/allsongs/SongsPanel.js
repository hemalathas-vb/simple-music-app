import { useEffect, useState } from 'react';
import SearchBar from './SearchBar'

  //  ENTER key is pressed search the songs list
  const checkIfEnterPressed = (e) => {
    if (e.which == 13 || e.keyCode == 13) {
      modifySongs()
    }
  }

  // modify the list of songs based on searched text
  const modifySongs = () => {
    let toBeSearchedSongs = [...props.songs]
    const searchValue = songData.searchText.toLowerCase()
  }

  const { changeRating } = props

  return (
    <div className="show-songs">
      <SearchBar
        searchText={searchText}
        clearSerchText={clearSerchText}
        checkIfEnterPressed={checkIfEnterPressed}
        changeSearchText={changeSearchText} />

      <div className="song-list">
        {
          (songs.length > 0) ?
            songs.map((song) => {
              return <Song key={song._id} {...song} changeRating={changeRating} />
            })
            :
            emptyMessage
        }
      </div>
    </div>
  );

export default SongsPanel;
