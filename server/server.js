import path, { dirname } from 'path'
const __dirname = dirname(__filename)

// serve the static pages
app.use(express.static(path.join(__dirname, '../public/dist')))


